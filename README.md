# Wordpress + Mysql + Adminer

Archivos para desplegar de manera local una instancia de wordpress, usando mysql como SGBD y adminer (para la administración gráfica de la BD)
NO usar en entornos de producción.

## Instalación

`git clone https://gitlab.com/luirro777/wp-adminer`

`cd wp-adminer`

`docker-compose up -d`

Este repositorio incluye un archivo php.ini, que sirve para cualquier configuración extra que sea necesaria.


## Acceso

Wordpress: http://localhost:8080

Adminer: http://localhost:8081

(credenciales de acceso en archivo docker-compose.yml)



